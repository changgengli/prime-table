#!/usr/bin/ruby


#A class for prime multiply table
class PrimeTable

  # Create a multiple table instance with first 'n' prime numbers
  def initialize(n)
    @top = PrimeTable.gen_prime(n)
    @top.unshift(1) #put 1 before all the prime numbers
    @size = n

    column_width = @top.collect { |e|
      (e * @top[-1]).to_s.length
    }
    @format_str = column_width.map { |w| "%#{w}d" }.join(' ')

  end


  # return the number at the specified row and column
  # in the multiply table
  # return 1 for the top-left position(0,0)

  def get(row, col)
    @top[col] * @top[row]
  end

  # the format template for this multiplication table
  def format_str
    @format_str
  end


  # print the multiply table to the specified output stream
  def pretty_print(out=STDOUT)
    rows.each { |r| puts r }
  end


  # return an enumerator for each row
  def rows
    (0 .. @size).map { |row|
      numbers = (0..@size).map { |col|
        get(row, col)
      }
      sprintf(@format_str, * numbers)
    }

  end

  #generate the first n prime numbers
  # gen_prime(2) => [2,3]
  def self.gen_prime(n)
    primes = [2, 3]
    while primes.length < n do
      primes.push(next_prime primes)
    end
    if primes.length > n
      primes.slice(0,n)
    else
      primes
    end
  end

  # check whether a candidate number is a prime
  #  * candidate: the number to check
  #  * primes: all known prime numbers less than candidate
  #  is_prime(3, [2]) => true
  #  is_prime(4, [2,3]) => false
  def self.is_prime(candidate, primes)
    square_root = Math.sqrt(candidate).to_i
    primes.each { |p|
      return true if (p > square_root)
      return false if (candidate % p ==0)
    }
    fail "shouldn't reach here #{candidate}, #{primes.join(',')}"
  end

  private_class_method :is_prime

  #figure out the next prime number based on all known prime numbers
  # self.next_prime([2,3,5]) => 7
  def self.next_prime(primes)
    start = primes[-1] +2
    while not is_prime(start, primes) do
      start = start + 2
    end
    start
  end

  private_class_method :next_prime

end

if __FILE__ == $PROGRAM_NAME
  n = 10
  n = ARGV[0].to_i if ARGV.length != 0
  PrimeTable.new(n).pretty_print(STDOUT) if n >=1
end
