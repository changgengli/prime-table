#!/usr/bin/ruby

require 'test/unit'
require_relative 'prime_table'
class PrimeTableTest < Test::Unit::TestCase


  def test_gen_prime
    list = [2, 3, 5, 7, 11]
    (list.length + 1).times { |index|
      assert_equal list.slice(0, index), PrimeTable.gen_prime(index), "Test gen_prime(#{index})"
    }
  end


  def test_get
    table = PrimeTable.new(3)
    assert_equal 1, table.get(0, 0)
    assert_equal 2, table.get(0, 1)
    assert_equal 2, table.get(1, 0)
    assert_equal 3, table.get(2, 0)
    assert_equal 9, table.get(2, 2)
    assert_equal 15, table.get(3, 2)
  end

  def test_column_format
    #first row should be 1 2 3 5
    #column width should be 1 2 2 2
    table = PrimeTable.new(3)
    assert_equal "%1d %2d %2d %2d", table.format_str

    table = PrimeTable.new(1)
    assert_equal "%1d %1d", table.format_str


  end

  def test_table_1
    table = PrimeTable.new(1)
    #should have 4 rows
    rows = table.rows
    #1 2
    #2 4
    assert_equal 2, rows.length
    assert_equal "1 2", rows[0]
    assert_equal "2 4", rows[1]

  end


  def test_rows
    table = PrimeTable.new(3)
    #should have 4 rows
    rows = []
    table.rows.each { |r|
      rows.push r
    }

    #1  2  3  5
    #2  4  6 10
    #3  6  9 15
    #5 10 15 25
    assert_equal 4, rows.length

    assert_equal "1  2  3  5", rows[0]
    assert_equal "2  4  6 10", rows[1]
    assert_equal "3  6  9 15", rows[2]
    assert_equal "5 10 15 25", rows[3]
  end

end